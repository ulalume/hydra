#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import math, os
import matplotlib.pyplot as plt

class one_dimensional:
    def __init__(self, time_interval = (0.0, 2.0), dt = 0.01, x_interval = (1.0, 2.0), dx = 0.01, u_data = None, velocity = 0.1, diffusion = 0.05, scheme = "burgers"):
        
        N = abs(x_interval[1] - x_interval[0]) / dx
        
        self.dt = dt
        self.t = time_interval[0]
        self.t_max = time_interval[1]
        
        self.dx = dx
        self.x = np.linspace(x_interval[0], x_interval[1], N)
        
        if(u_data == None):
            self.u = np.zeros(N)
            self.u[np.where(self.x < 1.5)[0]] = 1.0
        else:
            self.u = u_data
        
        self.velocity = velocity
        self.diffusion = diffusion
        
        self.scheme = scheme
        
        # create output directories
        if(os.path.isdir("./plot") == False):
            os.mkdir("./plot")
        if(os.path.isdir("./data") == False):
            os.mkdir("./data")
    
    def set_dt(self, dt = None):
        if(dt != None):
            self.dt = dt
    
    def set_dx(self, dx = None):
        if(dx != None):
            self.dx = dx
    
    ## atd. další možné nastavování parametrů
    
    def solve(self):
        count = 0
        self.plot(count)
        
        while(self.t <= self.t_max):
            tmp = []
            
            i = 0
            while(i < len(self.x)):
                if(i == 0): # dolni okrajova podminka
                    tmp.append(self.u[i])
                elif(i == len(self.x) - 1): # horni okrajova podminka
                    tmp.append(self.u[i])
                else: # vsechny ostani hodnoty (resit normalne pomoci schematu)
                    tmp.append(self.scheme_functions(i))
                i+= 1
            self.u = np.array(tmp)
            
            # inkrementovat cas
            self.t += self.dt
            count += 1
            
            self.plot(count)
    
    def scheme_functions(self, i):
        if(self.scheme == "upwind"):
            return self.u[i] - ((self.velocity * self.dt) / self.dx) * (self.u[i+1] - self.u[i-1]) # Up-Wind scheme
        elif(self.scheme == "upwind_diff"):
            return self.u[i] - ((self.velocity * self.dt) / self.dx) * (self.u[i+1] - self.u[i-1]) + (self.diffusion * self.dt / math.pow(self.dx, 2)) * (self.u[i+1] - 2.0 * self.u[i] + self.u[i-1]) # Up-Wind scheme + Diffusion
        elif(self.scheme == "fdtcda"):
            return self.u[i] - ((self.velocity * self.dt) / (2.0 * self.dx)) * (self.u[i+1] - self.u[i-1]) # Finite Diference Time Central Diference Advection
        elif(self.scheme == "lax"):
            return (self.u[i-1] + self.u[i+1]) / 2.0 - ((self.velocity * self.dt) / (2.0 * self.dx)) * (self.u[i+1] - self.u[i-1]) # Lax scheme
        elif(self.scheme == "burgers"):
            return (self.u[i-1] + self.u[i+1]) / 2.0 - ((self.u[i] * self.dt) / (2.0 * self.dx)) * (self.u[i+1] - self.u[i-1]) # Burgers scheme
    
    def plot(self, i):
        fig = plt.figure(figsize=(10, 10))
        ax1 = fig.add_subplot(1,1,1)
        
        ax1.plot(self.x, np.abs(self.u), linestyle="-", color="blue")
        ax1.set_xlabel("$x$", fontsize = 15)
        ax1.set_ylabel("$u$", fontsize = 15)
        ax1.set_xlim([1.0, 2.0])
        ax1.set_ylim([-0.1, 2.0])
        
        plt.grid()
        plt.savefig("./plot/frame_"+str(i)+".png")
        plt.close(fig)

if(__name__ == '__main__'):
    hd = hydra(scheme = "lax")
    
    hd.solve()

