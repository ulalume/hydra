#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import numpy as np
import matplotlib.pyplot as plt
 

# grid a parametry
N = 100
x = np.zeros(N)
u = np.zeros(N)
x_min = 1.0
x_max = 2.0
dx = (x_min+x_max) / N
dt = 0.01
t = 0.0
t_end = 2.0
v = 0.1

# pocatecni podminky
x = np.linspace(x_min, x_max, N)
u[np.where(x<1.5)[0]] = 1.0

# vykresleni grafu
def plot(x, u, n):
    fig = plt.figure(figsize=(12,12))
    ax1 = fig.add_subplot(1,1,1)
    ax1.plot(x, u, linestyle="-", color="red")
    ax1.set_xlabel("x", fontsize = 14)
    ax1.set_ylabel("u", fontsize = 14)
    ax1.set_xlim([1.0, 2.0])
    ax1.set_ylim([-1.1, 2.1])
    plt.grid()   
    plt.savefig("./grafy/graf-"+str(n)+".png")
    plt.close(fig) 

# vykresli graf pocatecnich podminek
n = 0 
plot(x, u, n) 

# vyber schematu
schema = "burgers" 

while(t <= t_end):
     
    reseni = []
     
    i = 0
    while(i < len(x)):
        if(i == 0): 
            reseni.append(u[i])
        elif(i == len(x) - 1):
            reseni.append(u[i])
        else: 
             
            if(schema == "up-wind"):
                reseni.append(u[i]-((u[i+1]-u[i-1])*(v*dt)/dx))
            elif(schema == "lax"):
                reseni.append((u[i-1]+u[i+1])*0.5-((u[i+1]-u[i-1])*0.5*(v*dt)/dx))
            elif(schema == "burgers"):
                reseni.append((u[i-1]+u[i+1])*0.5-((u[i+1]-u[i-1])*(u[i]*dt*0.5)/dx))
             
        i = i + 1
    u = np.array(reseni)  
    t = t + dt
    n = n + 1
     
 
    plot(x, u, n) 

