import numpy
import matplotlib.pyplot # knihovna - vykreslovanie
# kod: zamena v za u 

class simulacia:
	def __init__(self): # povinny nazov/cast, self - zada lokalne premenne
		# self - vola sa sama do seba / bezi vnutri
		# parametry
		self.x0 = 1.0 # miesto x inc (1.2)
		self.xend = 2.0 # pozn. python sam pozna integer a real
		self.t = 0.0
		self.tend = 2.0
		self.dt = 0.1 # poct krokov v simulacii
		self.N = 100 # pocet deleni
		self.v = 0.1 # zadana rychlost
		self.dx = (self.xend-self.x0)/self.N # rozsekame

		# grid
		self.x = numpy.zeros(self.N) # vytvori pole plne nul v dlzke N, potom menime prvky
		# numpy.array - vytvori prazdne pole, ones - jednotky
		self.u = numpy.zeros(self.N)


	def zadanie_hodnot(self):    # do poli

		self.x = numpy.linspace (1,2,self.N) # dany interval rozseka na N casti
			# linsapce - vhodnejsie ako arange kvoli necelociselnemu kroku
		
		# pociatocna hodnota u
		i = 0 # index prveho prvku
		while(i < len(self.x)): # len - dlzka 
			if(self.x[i] <= 1.5):
				self.u[i] = 1.0
			else:
				self.u[i] = 0.0
			i = i + 1

		# u[numpy.where(x <= 1.5)[0]]=1 da sa pouzit miesto predchadzajuceho cyklu
			# [] identifikacia indexu konkretneho prvku v poli

	def riesenie(self):
		poradie_snimku = 0

		while(self.t <= self.tend):
			# docasne data
			docasne = [] # vytvori prazdne obecne pole kam sa daju ukladat hodnoty

			# riesenie pre rozne podmienky
			i = 0
	
			while(i < len(self.x)):
				if(i == 0): # dolna podmienka
					docasne.append(self.u[i]) # prida prvok na koniec pola
				elif(i == len(self.x)-1): # horna podmienka
					docasne.append(self.u[i]) #
				else: # dalsie pomocou schematu riesenia
					docasne.append(self.schema(i))
				i = i +1

			self.u = numpy.array(docasne)

			# zvysovanie casu
			self.t = self.t + self.dt

			poradie_snimku = poradie_snimku + 1
			self.plot(poradie_snimku) #cyklicky sype obrazky

	def schema(self, a): # vzdy pre jednu hodnotu "a" pre schema(i)
		#return (self.u[a]-((self.v*self.dt)/self.dx)*(self.u[a]-self.u[a-1])) #up-wind scheme
		return (self.u[a]-((self.u[a]*self.dt)/self.dx)*(self.u[a]-self.u[a-1])) #zamena v za u 

	def plot(self, poradie_snimku):
		fig = matplotlib.pyplot.figure(figsize=(10,10)) # vyploti obrazok
		ax1 = fig.add_subplot(1,1,1) # zakladne nastavenie

		ax1.plot(self.x, self.u, linestyle="-",color="blue") #graf
		
		matplotlib.pyplot.grid()
		matplotlib.pyplot.savefig("frame_"+str(poradie_snimku)+".png")
		matplotlib.pyplot.close(fig)

# HLAVNE TELO PROGRAMU
# vytvori instanciu triedy pri behu programu
simulacia = simulacia()

# pociatocne hodnoty
simulacia.zadanie_hodnot()

# prebehne riesenie
simulacia.riesenie()
