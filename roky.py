#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

class hydro:
    def __init__(self):
        # nastaveni parametru
        self.x_interval = [1.0, 2.0]
        self.dt = 0.01
        self.t = 0.0
        self.t_end = 2.0
        self.N = 100
        self.v = -0.1
        self.dx = (self.x_interval[1] - self.x_interval[0]) / self.N
        
        # grid
        self.x = np.zeros(self.N)
        self.u = np.zeros(self.N)
    
    def values_init(self):
        # pocatecni hodnoty x
        self.x = np.arange(1, 2, self.dx)
        
        # pocatecni hodnoty u
        i = 0
        while(i < len(self.x)):
            if(self.x[i] <= 1.5):
                self.u[i] =  1
            else:
                self.u[i] =  0
            i = i + 1
    
    def solve(self):
        count = 0
        self.plot(count)
        
        while(self.t <= self.t_end):
            # docasna data
            tmp = []
            
            # reseni (pozn. horni a dolni podminka nemusi byt stejna proto jsou oddelene)
            i = 0
            
            while(i < len(self.x)):
                if(i == 0): # dolni okrajova podminka
                    tmp.append(self.u[i])
                elif(i == len(self.x) - 1): # horni okrajova podminka
                    tmp.append(self.u[i])
                else: # vsechny ostani hodnoty (resit normalne pomoci schematu)
                    tmp.append(self.scheme(i))
                i = i + 1
            
            self.u = np.array(tmp)
            
            # inkrementovat cas
            self.t = self.t + self.dt
            
            count = count + 1
            self.plot(count)
    
#    def scheme(self, i):
#        return (self.u[i-1] - self.u[i+1]) / 2.0 - ((self.v * self.dt) / (2.0 * self.dx)) * (self.u[i+1] - self.u[i-1])


    def scheme(self, i):
        return (self.u[i]-((self.v*self.dt)/self.dx)*(self.u[i]-self.u[i-1]))


    def plot(self, i):
        fig = plt.figure(figsize=(10, 10))
        ax1 = fig.add_subplot(1,1,1)
        
        ax1.plot(self.x, self.u, linestyle="-", color="blue")
        ax1.set_xlabel("$x\\ [m]$", fontsize = 15)
        ax1.set_ylabel("$u\\ [m]$", fontsize = 15)

        plt.grid()
        #plt.show()
        plt.savefig("frame_"+str(i)+".png")
        plt.close(fig)

# vytvorit instanci tridy hydro
hydro = hydro()

# nastaveni pocatecnich hodnot
hydro.values_init()

# provest reseni
hydro.solve()

