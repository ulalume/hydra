import numpy as np

def reseni_rovnice():

	vysledek = []

	N = 100
	T0 = 0.0
	Tend = 2.0
	pocatek = 1.0
	konec = 2.0

	deltaT = 0.01
	deltaX = (konec-pocatek)/N
	v = 0.1

	X = np.linspace(1,2,N)
	"""
	U = [0.0 for i in range(0,N)]

	#pocatecni podminka
	for m in range(0,51):
		U[m] = 1.0

	"""	
	U = np.zeros(N)
	U[np.where(X<1.5)[0]] = 1.0

	vysledek.append(U[::])

	T = T0
	while T < Tend:
		for i in range(1,N-1):

			U[i] = (0.5)*(U[i+1] + U[i-1]) - (U[i+1] - U[i-1]) * ((v*deltaT)/(2.0*deltaX))

			U[0] = U[0]
			U[N-1] = U[N-1]


		vysledek.append(U[::])
		T += deltaT

	f = open("rovnice.txt","w")
	for i in range(0,N):
		f.write(str(X[i]))
		for j in range(0,N):
			f.write("\t")
			a = vysledek[j]
			f.write(str(a[i]))
		f.write("\n")
	f.close()

reseni_rovnice()


