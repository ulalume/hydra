#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Numericka metoda na riesenie dif. rovnice podobnej r. kontinuity (objektovo)

#import kniznic
import numpy as np
from matplotlib import pyplot as plt


class Hydro:
	#konstruktor s parametrami objektu
	def __init__(self,N):
		self.N = N
		self.x = np.linspace(1,2,self.N)
		self.u = np.zeros(self.N)
		self.dt = 0.01
		self.t0 = 0
		self.tend = 2.0
		self.xmin = 1.0
		self.xmax = 2.0
		self.v = 0.1
		self.dx = (self.xmax - self.xmin)/self.N
		

	def initial(self):
		self.u[np.where(self.x<1.5)[0]] = 1.0
		return self.u


	def upwind_scheme(self):
		for i in range(self.N-1):
			self.u[i] = (self.u[i+1]+self.u[i-1])*0.5 - (self.u[i+1]-self.u[i-1])*0.5*self.v*self.dt/self.dx
		return self.u


	def boundary(self):
		self.u[0] = self.u[0]
		self.u[self.N-1] = self.u[self.N-1]
		return self.u


	def solution(self):
		self.initial()		
		self.t = 0
		while (self.t < self.tend):
			self.upwind_scheme()
			self.boundary()
			self.t = self.t + self.dt
		return self.u


rovnice = Hydro(100)
print rovnice.solution()

# graf
plt.plot(rovnice.x, rovnice.u, 'r')
plt.xlabel("suradnica x")
plt.ylabel("funkcia u")
plt.axis([1.0,2.0,-1.1,1.1])
plt.grid(True)

plt.show()




