#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Numericka metoda na riesenie dif. rovnice podobnej r. kontinuity

#import kniznic
import numpy as np
from matplotlib import pyplot as plt

#parametre problemu

#kroky
N = 100
#tok casu
t0 = 0
tend = 2.0
dt = 0.01
t = t0
#rychlost
v = 0.1
#suradnica
xmin = 1.0
xmax = 2.0
dx = (xmax - xmin)/N

#ekvidistancne hodnoty (od, do, pocet krokov)
x = np.linspace(xmin,xmax,N)
#vytvori pole s nulovymi hodnotami
u = np.zeros(N)

#vsetkym x podla prepisu priradi hodnotu,[0] urobi pole
u[np.where(x<1.5)[0]] = 1.0



while (t < tend):
	# Laxova schema, nestabilna
	for i in range(1,99):
		u[i] = (u[i+1]+u[i-1])*0.5 - (u[i+1]-u[i-1])*0.5*v*dt/dx
	# okrajove podmienky
	u[0] = u[0]
	u[N-1] = u[N-1]
	t = t + dt

#graf
plt.plot(x, u, 'r')
plt.xlabel("suradnica (x)")
plt.ylabel("funkcia (u)")
plt.axis([1.0,2.0,-1.1,3.1])
plt.grid(True)
plt.show()
