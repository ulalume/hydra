#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import math
import matplotlib.pyplot as plt

class hydro:
    def __init__(self):
        # parametry
        self.x_interval = [1.0, 2.0]
        self.dt = 0.01
        self.t = 0.0
        self.t_end = 2.0
        self.N = 100
        self.v = 0.1
        self.dx = (self.x_interval[1] - self.x_interval[0]) / self.N
        self.D = 0.05
        
        # grid
        self.x = np.zeros(self.N)
        self.u = np.zeros(self.N)
        
        # pouzite schema
        self.mode = "burgers"
    
    def values_init(self):
        # pocatecni hodnoty x
        self.x = np.linspace(self.x_interval[0], self.x_interval[1], self.N)
        
        # pocatecni hodnoty u
        sin_x = np.linspace(math.radians(0), math.radians(360), self.N / 2)
        i = 0
        while(i < len(self.x)):
            
            if(self.x[i] <= 1.25):
                self.u[i] = 1.0
            elif(self.x[i] > 1.25 and self.x[i] < 1.75 ):
                self.u[i] = 0.0
            elif(self.x[i] >= 1.75):
                self.u[i] = -1.0
            
            
            """
            if(self.x[i] <= 1.5):
                self.u[i] =  1.0
                #self.u[i] =  (math.sin(sin_x[i]) / 2.0) + 1
            else:
                self.u[i] =  0.0
            """
            i = i + 1
    
    def solve(self):
        count = 0
        self.plot(count)
        
        while(self.t <= self.t_end):
            # docasna data
            tmp = []
            
            # reseni (pozn. horni a dolni podminka nemusi byt stejna proto jsou oddelene)
            i = 0
            while(i < len(self.x)):
                if(i == 0): # dolni okrajova podminka
                    tmp.append(self.u[i])
                elif(i == len(self.x) - 1): # horni okrajova podminka
                    tmp.append(self.u[i])
                else: # vsechny ostani hodnoty (resit normalne pomoci schematu)
                    tmp.append(self.scheme(i))
                i = i + 1
            self.u = np.array(tmp)
            
            # inkrementovat cas
            self.t = self.t + self.dt
            
            count = count + 1
            self.plot(count)
    
    def scheme(self, i):
        if(self.mode == "upwind"):
            return self.u[i] - ((self.v * self.dt) / self.dx) * (self.u[i+1] - self.u[i-1]) # Up-Wind scheme
        elif(self.mode == "upwind_diff"):
            return self.u[i] - ((self.v * self.dt) / self.dx) * (self.u[i+1] - self.u[i-1]) + (self.D * self.dt / math.pow(self.dx, 2)) * (self.u[i+1] - 2.0 * self.u[i] + self.u[i-1]) # Up-Wind scheme + Diffusion
        elif(self.mode == "fdtcda"):
            return self.u[i] - ((self.v * self.dt) / (2.0 * self.dx)) * (self.u[i+1] - self.u[i-1]) # Finite Diference Time Central Diference Advection
        elif(self.mode == "lax"):
            return (self.u[i-1] + self.u[i+1]) / 2.0 - ((self.v * self.dt) / (2.0 * self.dx)) * (self.u[i+1] - self.u[i-1]) # Lax scheme
        elif(self.mode == "burgers"):
            return (self.u[i-1] + self.u[i+1]) / 2.0 - ((self.u[i] * self.dt) / (2.0 * self.dx)) * (self.u[i+1] - self.u[i-1]) # Burgers scheme
    
    def plot(self, i):
        fig = plt.figure(figsize=(10, 10))
        ax1 = fig.add_subplot(1,1,1)
        
        ax1.plot(self.x, np.abs(self.u), linestyle="-", color="blue")
        ax1.set_xlabel("$x$", fontsize = 15)
        ax1.set_ylabel("$u$", fontsize = 15)
        ax1.set_xlim([1.0, 2.0])
        ax1.set_ylim([-0.1, 2.0])
        
        plt.grid()
        plt.savefig("./plot/frame_"+str(i)+".png")
        plt.close(fig)

# vytvorit instanci tridy hydro
hydro = hydro()

# nastaveni pocatecnich hodnot
hydro.values_init()

# provest reseni
hydro.solve()

