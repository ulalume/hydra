#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hydra as hyd

h = hyd.one_dimensional(scheme = "lax", dx = 0.001)
h.solve()