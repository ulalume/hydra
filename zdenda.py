#!/usr/bin/env python
# -*- coding: utf-8 -*-
""""""


import numpy as np
import matplotlib.pyplot as plt


def graf(x, u, t):
    """Vykresli potencial zavysly na poloze v danem casovem okamziku"""
    plt.clf()
    plt.grid()
    plt.xlim(1, 2)
    plt.ylim(-0.1, 1.1)
    plt.xlabel("x")
    plt.ylabel("u")
    plt.title("Time = {:1.2}".format(t))
    plt.plot(x, u)
    plt.savefig("plt-{:1.2}.png".format(t))


## parametry
dt = 0.01
dx = 0.01
v = 0.1
N = 100
T0 = 0
Tend = 2
X0 = 1
Xend = 2

## pocatecni podminky
x = np.arange(X0, Xend+dx, dx)
t = np.arange(T0, Tend+dt, dt)
u = np.ndarray(len(x))
u[x <= 1.5] = 1
u[x > 1.5] = 0

## casovy vyvoj
un = u.copy()
for ti in t:
    graf(x, un, ti)
    for i in range(1, N-1):
        u[i] = 0.5 * (un[i+1] + un[i-1]) - 0.5 * v * dt / dx * (un[i+1] - un[i-1])
    un = u.copy()


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
